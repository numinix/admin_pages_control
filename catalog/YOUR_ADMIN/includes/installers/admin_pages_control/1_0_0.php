<?php
$configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_title = 'Admin Pages Control Configuration' ORDER BY configuration_group_id ASC;");
if ($configuration->RecordCount() > 0) {
  while (!$configuration->EOF) {
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $db->Execute("DELETE FROM " . TABLE_CONFIGURATION_GROUP . " WHERE configuration_group_id = " . $configuration->fields['configuration_group_id'] . ";");
    $configuration->MoveNext();
  }
}

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION_GROUP . " (configuration_group_title, configuration_group_description, sort_order, visible) VALUES ('Admin Pages Control Configuration', 'Set Admin Pages Control Configuration Options', '1', '1');");
$configuration_group_id = $db->Insert_ID();

$db->Execute("UPDATE " . TABLE_CONFIGURATION_GROUP . " SET sort_order = " . $configuration_group_id . " WHERE configuration_group_id = " . $configuration_group_id . ";");

$db->Execute("INSERT INTO " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, last_modified, date_added, use_function, set_function) VALUES
            ('Version', 'ADMIN_PAGES_CONTROL_VERSION', '1.0.0', 'Version installed:', " . $configuration_group_id . ", 0, NOW(), NOW(), NULL, NULL);");

$zc150 = (PROJECT_VERSION_MAJOR > 1 || (PROJECT_VERSION_MAJOR == 1 && substr(PROJECT_VERSION_MINOR, 0, 3) >= 5));
if ($zc150) { // continue Zen Cart 1.5.0
  // delete configuration menu
  $db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configAdminPagesControlConfiguration' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configAdminPagesControlConfiguration')) {
    $configuration = $db->Execute("SELECT configuration_group_id FROM " . TABLE_CONFIGURATION . " WHERE configuration_key = 'ADMIN_PAGES_CONTROL_VERSION' LIMIT 1;");
    $configuration_group_id = $configuration->fields['configuration_group_id'];
    if ((int)$configuration_group_id > 0) {
      zen_register_admin_page('configAdminPagesControlConfiguration',
                              'BOX_CONFIGURATION_ADMIN_PAGES_CONTROL', 
                              'FILENAME_CONFIGURATION',
                              'gID=' . $configuration_group_id, 
                              'configuration', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Admin Pages Control Configuration menu.', 'success');
    }
  }
  $db->Execute("DELETE FROM " . TABLE_ADMIN_PAGES . " WHERE page_key = 'configAdminPagesControlAccess' LIMIT 1;");
  // add configuration menu
  if (!zen_page_key_exists('configAdminPagesControlAccess')) {
    
      zen_register_admin_page('configAdminPagesControlAccess',
                              'BOX_ACCESS_ADMIN_PAGES_CONTROL', 
                              'FILENAME_ADMIN_PAGES_CONTROL',
                              '', 
                              'access', 
                              'Y',
                              $configuration_group_id);
        
      $messageStack->add('Enabled Admin Pages Control Access menu.', 'success');
    
  }
}
