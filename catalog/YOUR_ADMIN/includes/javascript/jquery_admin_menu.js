
jQuery(function () {

	// global
    var $buttonAddPage = jQuery(".nmx-add-page"),
    	$buttonAddMenu = jQuery("#nmx-add-menu"),
    	$listPages = jQuery(".nmx-pages"),
    	$listTabs = jQuery("#nmx-list-tabs");

    // functions
    var tabs = function(ul) {

    	ul.on('click', 'a', function(event) {
			
    		var $this = jQuery(this);
    			$tabItem = $this.parent(),
    			$tabContent = jQuery(".nmx-tab");
    			$selectedTab = $this.attr("href");

    		// unable multiple clicks
    		if (!$tabItem.hasClass("active")) {

    			// add menu has your own way
    			if(!$this.hasClass("nmx-btn-add")) {

    				//	remove active / set it as old-active
    				$tabItem.parent().find('li').each(function(i) {
					
						jQuery(this).removeClass("last-active");

						if (jQuery(this).hasClass("active")) {
							jQuery(this).addClass("last-active");
						}
						
						jQuery(this).removeClass("active");

					});

					$tabItem.addClass("active");
    			}

				//	Hide all tab content
				$tabContent.hide();

				jQuery($selectedTab).show();
    		
    		}

			event.preventDefault();

		});

		ul.on('click', '.nmx-link-new-tab-menu', function(event) {
			$("#nmx-field-menu-title").find("input").focus();
		});

    }

    
    var addNewPage = function(){
		
		$buttonAddPage.on('click', function(event){
			
			var $this = jQuery(this);

		 	// unable multiple clicks
		 	if(!$this.hasClass("is-clicked")) {

		 		var $listPages = $this.next();
		 			$firstPage = $listPages.find('li:first-child'),
			 		$firstPageClone = $firstPage.clone(),
			 		$newPageId = "create-page-added";
                                        
                                        
		 		// insert clone before the first position
		 		$firstPageClone.insertBefore($firstPage);

		 		// new first position highlithed
		 		$listPages
		 			.find('li:first-child')
		 			.css({
		 				"opacity": ".6"
		 			})
		 			.addClass("is-active")
		 			.animate({
				    	opacity: 1
				  	}, 250, function() {
				    	// Animation complete.
				  	})
		 			.attr("id", $newPageId);
                                
		 		$this.addClass("is-clicked");

		 		$listPages.addClass("is-blur");

		 		contentNewPage($newPageId);
                                
                                
		 	}

		  	event.preventDefault();
		});

	};

	var contentNewPage = function(id) {
		
		var $this = jQuery("#" + id),
			$input = $this.find("input");

		// change title to an input
		$this
			.find("h3")
			.html("(Specify Language Key)");

		// clean all fields
		$input.each(function(){ 
                    if(jQuery(this).attr("name") != "securityToken"){
			jQuery(this).attr("value", "");
                    }
		});
                
                
                
		// buttons
		$this
			.find(".nmx-btn-update-page")
			.html("Create Page")
			.removeClass("nmx-btn-update-page")
			.addClass("nmx-btn-create-page"); // when clicked calls createNewPage

		$this
			.find(".nmx-btn-remove-page")
			.html("Cancel")
			.removeClass("nmx-btn-remove-page")
			.addClass("nmx-btn-cancel-page") // when clicked calls cancelAddNewPage
			.attr("type", "reset");	
                
                //fill hidden Values
                $this 
                        .find("input[name=action]:hidden")
                        .attr('value','add');
                $this 
                        .find("input[name=change]:hidden")
                        .attr('value','page');
                
                // focus on first
		$this.find("input[name='page_key']").focus();
	};

	var cancelAddNewPage = function() {
		$listPages.on('click', '.nmx-btn-cancel-page', function(event){
			
			// remove item and unblur list
			$listPages.find("li:first-child").remove();
			$listPages.removeClass("is-blur");
			
			// enable button add page
			$buttonAddPage.removeClass("is-clicked");

		});
	};

	var createNewPage = function() {
		$listPages.on('click', '.nmx-btn-create-page', function(event) {
			// backend here for creating the new page
		});
	};

	var addNewMenu = function() {
		
		$buttonAddMenu.on('click', function(event) {
			
			var $this = jQuery(this);

			// unable multiple clicks
			if(!$this.hasClass("is-clicked")) {
				contenNewMenu();
				$this.addClass("is-clicked");
				event.preventDefault();
			}

		});

	};

	var contenNewMenu = function() {

		// create tab
		jQuery("<li class='nmx-new-tab-menu nmx-tab-menu'><a class='nmx-link-new-tab-menu' href='#nmx-tab-add-menu'><span>Untitled</span></a></li>")
				.insertBefore("#nmx-list-tabs li:last-child");

		$listTabs
			.find(".active")
			.removeClass("active");

		// activate new tab
		$listTabs
			.find(".nmx-new-tab-menu")
			.addClass("active");

	};

	var cancelAddNewMenu = function() {

		jQuery("#nmx-btn-cancel-menu").on("click", function() {

			// remove new tab
			$listTabs
				.find(".nmx-new-tab-menu")
				.remove();

			// enable button
			$buttonAddMenu.removeClass("is-clicked");

			// back to the last before trying to add new menu
			// $listTabs.find(".last-active").find("a").trigger("click");
			$listTabs.find("li:first-child").find("a").trigger("click");


		});

	}

	var changingTabs = function(){
		$listTabs.find(".nmx-tab-menu").on("click", function () {
			
		});
	}


    // init
    var init = function(){
    	// tabs
    	tabs($listTabs);

		// add page functions
		addNewPage();
		cancelAddNewPage();
		createNewPage();

		// add menu functions
		addNewMenu();
		cancelAddNewMenu();
    };
	
	// start all
	init();

});
