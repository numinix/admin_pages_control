<?php
/**
 * @package admin
 * @copyright Copyright 2003-2011 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: admin_page_registration.php 18695 2011-05-04 05:24:19Z drbyte $
 */

require('includes/application_top.php');
switch($_POST['action']){
    //update pages/menus
        case update:
            switch($_POST['change']){
                case page:
                    $db->Execute("UPDATE ".TABLE_ADMIN_PAGES." SET main_page='".$_POST['main_page']."',`page_params`='".$_POST['page_params']."',`menu_key`='".$_POST['menu_key']."',`display_on_menu`='".$_POST['display_on_menu']."',`sort_order`='".$_POST['sort_order']."' WHERE page_key='".$_POST['page_key']."'");
                    $messageStack->add('Admin Page Updated', 'success');
                    break;
                case menu:
                    $db->Execute("UPDATE ".TABLE_ADMIN_MENUS." SET sort_order='".$_POST['sort_order']."' WHERE menu_key='".$_POST['menu_key']."'");
                    $messageStack->add('Admin Menu Updated', 'success');
                    break;
            }
            break;
    //add pages/menus    
        case add:
            switch($_POST['change']){
                case page:
                    $page_exist_query = $db->Execute("SELECT * FROM ".TABLE_ADMIN_PAGES." WHERE page_key='".$_POST['page_key']."'");
                    if($page_exist_query->RecordCount > 0){
                        $messageStack->add('An Admin Page Already has that page_key', 'caution');
                        break;
                    }
                    zen_register_admin_page($_POST['page_key'],
                                            $_POST['language_key'], 
                                            $_POST['main_page'],
                                            $_POST['page_params'], 
                                            $_POST['menu_key'],  
                                            $_POST['display_on_menu'],  
                                            $_POST['sort_order']);
                    $messageStack->add('Admin Page Added', 'success');
                    break;
                case menu:
                    $menu_exist_query = $db->Execute("SELECT * FROM ".TABLE_ADMIN_MENUS." WHERE menu_key='".$_POST['menu_key']."'");
                    if($menu_exist_query->RecordCount > 0){
                        $messageStack->add('A Menu Already has that menu_key', 'caution');
                        break;
                    }
                    $db->Execute("INSERT INTO ".TABLE_ADMIN_MENUS." "
                            . "(sort_order, menu_key, language_key)"
                            . "VALUES ('".$_POST['sort_order']."', '".$_POST['menu_key']."', '".$_POST['language_key']."')");
                    $messageStack->add('Admin Menu Updated', 'success');
                    break;
            }
            break;
        case remove:
            switch($_POST['change']){
                case page:
                    $db->Execute("DELETE FROM ".TABLE_ADMIN_PAGES." WHERE page_key='".$_POST['page_key']."'");
                    $messageStack->add('Admin Page Removed', 'success');
                    break;
                case menu:
                    $db->Execute("DELETE FROM ".TABLE_ADMIN_MENUS." WHERE menu_key='".$_POST['menu_key']."'");
                    $messageStack->add('Admin Menu Removed', 'success');    
                    break;
            }
                  
}



// prepare options for menu pulldown
$menus_dropdown = $db->Execute("SELECT * FROM ".TABLE_ADMIN_MENUS." ORDER BY sort_order");
while(!$menus_dropdown->EOF){
    $menu_options[] = array('id' => $menus_dropdown->fields['menu_key'], 'text' => $menus_dropdown->fields['menu_key']);
    $menus_dropdown->MoveNext();
}
$menu_titles = zen_get_menu_titles();
$menu_options = array();
foreach ($menu_titles as $id => $title) {
  $menu_options[] = array('id' => $id, 'text' => $title);
}

?>


<!doctype html public "-//W3C//DTD HTML 4.01 Transitional//EN">
<html <?php echo HTML_PARAMS; ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo CHARSET; ?>">
<title><?php echo TITLE; ?></title>
<link rel="stylesheet" type="text/css" href="includes/reset.css">
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="includes/stylesheet.css">
<link rel="stylesheet" type="text/css" href="includes/cssjsmenuhover.css" media="all" id="hoverJS">
<link rel="stylesheet" type="text/css" href="includes/admin_menu_control.css" />

<script language="javascript" src="includes/menu.js"></script>
<script language="javascript" src="includes/general.js"></script>
<script type="text/javascript">
  <!--
  function init() {
    cssjsmenu('navbar');
  }
  // -->
</script>
</head>
<body onload="init()">
<!-- header //-->
<?php require(DIR_WS_INCLUDES . 'header.php'); ?>
<!-- header_eof //-->

<!-- body //-->
<div id="pageWrapper" class="nmx-menu-control">
    <h1>Admin Menu Control</h1>
    
    


    <div class="nmx-menu">
        <ul class="nmx-list-tabs" id="nmx-list-tabs">
            <?php
                $menus = $db->Execute("SELECT * FROM ".TABLE_ADMIN_MENUS." ORDER BY sort_order");
                // start looping (menu) for where should be tabs
                $i=0;
                while(!$menus->EOF){
                
            ?>
                  
                      
                  
            <li class='nmx-tab-<?php echo $i ?> nmx-tab-menu <?php if ($i == 0) { ?>active<?php } ?>'><a href="#nmx-tab-<?php echo $i ?>"><span><?php echo constant($menus->fields['language_key']);?></span></a></li>
            <?php
                $i++;
                $menus->MoveNext();
            }?>

            <!-- tab for creating a new menu -->
            <li class="nmx-tab-last">
                <a href="#nmx-tab-add-menu" class="nmx-btn-link nmx-btn-add" id="nmx-add-menu">
                    <span>Add Menu</span>
                </a>
            </li>
        </ul>
        
    </div>
            
    <?php
        $menus = $db->Execute("SELECT * FROM ".TABLE_ADMIN_MENUS." ORDER BY sort_order");
        $i=0;
        // start looping (menu) for where should be tabs
        while(!$menus->EOF){
    ?>
    <div id='nmx-tab-<?php echo $i ?>' class="nmx-tab nmx-box">

    <!-- main item menu -->
        
        <!-- box with informations about the main item -->
        <div class="nmx-menu-details">
            <div class="nmx-form">
             <?php
                echo zen_draw_form('menu_change','admin_pages_control.php'); 
                echo zen_draw_hidden_field('action','remove');
                echo zen_draw_hidden_field('change','menu');
                echo zen_draw_hidden_field('menu_key',$menus->fields['menu_key']);
                ?>
                <!-- <?php echo zen_image_submit('button_remove.gif', IMAGE_REMOVE, 'id="button"') ?> -->
                <div class="nmx-field-buttons">                
                <button class="nmx-remove-menu">
                    <span>Remove Menu</span>
                </button>
                </div>
            </form>
            </div>
            <?php 
                echo zen_draw_form('menu_change','admin_pages_control.php'); 
                echo zen_draw_hidden_field('action','update');
                echo zen_draw_hidden_field('change','menu');
            ?>
                <div class="nmx-form nmx-box">
                    
                    <div class="nmx-row nmx-row-3">
                        <div class="nmx-field nmx-field-menu-key">
                            <label>Menu Key:</label>
                            <?php echo zen_draw_input_field('menu_key',$menus->fields['menu_key']);?>
                        </div>
                        <div class="nmx-field nmx-field-menu-language">
                            <label>Language Key:</label>
                            <?php echo zen_draw_input_field('language_key',$menus->fields['language_key']);?>
                        </div>
                        <div class="nmx-field nmx-field-menu-sort">
                            <label>Sort Order:</label>
                            <?php echo zen_draw_input_field('sort_order',$menus->fields['sort_order'])?>
                        </div>
                        <div class="nmx-field-buttons nmx-field-update-menu">
                        <!--  <?php echo zen_image_submit('button_update.gif', IMAGE_UPDATE, 'id="button"') ?> -->

                            <button class="nmx-btn">
                                Update Menu
                            </button>
                            
                        </div>
                    </div>  
                     
                </div>
            </form>
        </div>
        
        <!-- list all categories/submenus for this item -->
        <a href="#" class="nmx-btn-link nmx-btn-add nmx-add-page" id="nmx-add-page">
            <span>Add Page</span>
        </a>

        <ul class="nmx-pages" id="nmx-pages">
          
        
            <?php
            $pages = $db->Execute("SELECT * FROM ".TABLE_ADMIN_PAGES." WHERE menu_key='".$menus->fields['menu_key']."' ORDER BY sort_order");
            $page_id_number = 0;
            // start looping for where should be all the details about the categories for this menu
            while(!$pages->EOF){
                $page_id_number++;
                if($pages->fields['display_on_menu'] != "Y"){$page_display_style = 'style="color:red;"';}else{ $page_display_style = '';}
                ?>
            
            <li>

                <div class="nmx-form nmx-box">
                    <h3><?php 
                    if(constant($pages->fields['language_key'])){
                        echo constant($pages->fields['language_key']);
                    }?></h3>
                    <!-- form -->
                    <?php 
                      echo zen_draw_form('admin_change','admin_pages_control.php'); 
                      echo zen_draw_hidden_field('action','update');
                      echo zen_draw_hidden_field('change','page');
                      echo zen_draw_hidden_field('page_key',$pages->fields['page_key']);
                    ?>

                        <!-- fields -->
                        <div class="nmx-field">
                            <label>Page Key:</label>
                            <?php echo zen_draw_input_field('page_key',$pages->fields['page_key']);?>
                        </div>
                        
                        <div class="nmx-field">
                            <label>Language Key:</label>
                            <?php echo zen_draw_input_field('language_key',$pages->fields['language_key']);?>
                        </div>

                        <div class="nmx-field">
                            <label>Main Page:</label>
                            <?php echo zen_draw_input_field('main_page',$pages->fields['main_page']);?>
                        </div>

                        <div class="nmx-field">
                            <label>Page Parameters:</label>
                            <?php echo zen_draw_input_field('page_params',$pages->fields['page_params']); ?>
                        </div>

                        <div class="nmx-field">
                            <label>Menu Key:</label>
                            <?php echo zen_draw_pull_down_menu('menu_key', $menu_options, $pages->fields['menu_key']) ;?>
                        </div>

                        <div class="nmx-field nmx-field-rc">
                            <label>Display on Menu:</label> 
                            <?php
                            if($pages->fields['display_on_menu'] == "Y" ){
                               $checked = true;
                            }
                            else {
                               $checked = false;
                            }
                              echo zen_draw_checkbox_field('display_on_menu','Y',$checked);
                            ?>
                        </div>

                        <div class="nmx-field">
                            <label>Sort Order:</label>
                            <?php echo zen_draw_input_field('sort_order',$pages->fields['sort_order']);?>    
                        </div>
                        <!-- end/fields -->
                        
                        <!-- buttons -->
                        <div class="nmx-field-buttons">
                            <!-- <?php echo zen_image_submit(IMAGE_UPDATE, IMAGE_UPDATE, 'id="button"'); ?> -->

                            <button class="nmx-btn nmx-btn-update-page">
                                Update Page
                            </button>

                        
                        </div>
                        
                        <!-- end/buttons -->

                     </form>
                     <div class="nmx-field-buttons">
                             <?php
                                echo zen_draw_form('page_change','admin_pages_control.php'); 
                                echo zen_draw_hidden_field('action','remove');
                                echo zen_draw_hidden_field('change','page');
                                echo zen_draw_hidden_field('page_key',$pages->fields['page_key']);
                                ?>
                                <!-- <?php echo zen_image_submit('button_remove.gif', IMAGE_REMOVE, 'id="button"') ?> -->
                            <button class="nmx-btn nmx-btn-remove-page" id="nmx-btn-remove-page" type="submit">
                                Remove
                            </button>
                            <?php
                        echo '</form>';
                        ?> 
                     </div>
                     <!-- end/form -->
                </div>

            </li>
              <?php
              $pages->MoveNext();
            }
            if($page_id_number == 0){
                ?>
                <li>

                <div class="nmx-form nmx-box">
                    <h3>(Specify Language Key)</h3>
                    <!-- form -->
                    <?php 
                      echo zen_draw_form('admin_change','admin_pages_control.php'); 
                      echo zen_draw_hidden_field('action','add');
                      echo zen_draw_hidden_field('change','page');
                    ?>

                        <!-- fields -->
                        <div class="nmx-field">
                            <label>Page Key:</label>
                            <?php echo zen_draw_input_field('page_key');?>
                        </div>
                        
                        <div class="nmx-field">
                            <label>Language Key:</label>
                            <?php echo zen_draw_input_field('language_key');?>
                        </div>

                        <div class="nmx-field">
                            <label>Main Page:</label>
                            <?php echo zen_draw_input_field('main_page');?>
                        </div>

                        <div class="nmx-field">
                            <label>Page Parameters:</label>
                            <?php echo zen_draw_input_field('page_params'); ?>
                        </div>

                        <div class="nmx-field">
                            <label>Menu Key:</label>
                            <?php echo zen_draw_pull_down_menu('menu_key', $menu_options) ;?>
                        </div>

                        <div class="nmx-field nmx-field-rc">
                            <label>Display on Menu:</label> 
                            <?php
                            if($pages->fields['display_on_menu'] == "Y" ){
                               $checked = true;
                            }
                            else {
                               $checked = false;
                            }
                              echo zen_draw_checkbox_field('display_on_menu','Y',$checked);
                            ?>
                        </div>

                        <div class="nmx-field">
                            <label>Sort Order:</label>
                            <?php echo zen_draw_input_field('sort_order',$pages->fields['sort_order']);?>    
                        </div>
                        <!-- end/fields -->
                        
                        <!-- buttons -->
                        <div class="nmx-field-buttons">
                            <!-- <?php echo zen_image_submit(IMAGE_CREATE, IMAGE_CREATE, 'id="button"'); ?> -->

                            <button class="nmx-btn nmx-btn-update-page">
                                Update Page
                            </button>

                        
                        </div>
                        
                        <!-- end/buttons -->

                     </form>
                     <!-- end/form -->
                </div>

            </li>
              <?php
            }
            ?>
        </ul>
        <!-- end/list all categories -->

    </div>
    <!-- end/main item menu -->

    <?php
        $i++;
        $menus->MoveNext();
    }?>
    
    <div id='nmx-tab-add-menu' class="nmx-tab nmx-box">
        
        <?php
        echo zen_draw_form('add_menu','admin_pages_control.php');
        echo zen_draw_hidden_field('action','add');
        echo zen_draw_hidden_field('change','menu');
        ?>
        <form action="" method="post">
            <div class="nmx-form nmx-box nmx-form-add-menu">
                <div class="nmx-row nmx-row-3">
                    <div class="nmx-field nmx-field-menu-key">
                        <label>Menu Key:</label>
                        <?php echo zen_draw_input_field('menu_key',$pages->fields['menu_key']);?>
                    </div>
                    <div class="nmx-field nmx-field-menu-language">
                        <label>Language Key:</label>
                        <?php echo zen_draw_input_field('language_key',$pages->fields['language_key']);?>
                    <div class="nmx-field nmx-field-menu-sort">
                        <label>Sort Order:</label>
                        <?php echo zen_draw_input_field('sort_order',$pages->fields['sort_order']);?>
                    </div>
                </div>
                 
                 
                
                <div class="nmx-field-buttons">
                    <!-- <input type="image" src="includes/languages/english/images/buttons/button_update.gif" border="0" alt="Update" title=" Update " id="button"> -->

                    <button class="nmx-btn">
                        Create Menu
                    </button>
                    <button class="nmx-btn" id="nmx-btn-cancel-menu" type="reset">
                        Cancel
                    </button>
                </div>
            </div>
        </form>
        <!-- end: doesn't work.. needs backend -->

    </div>


</div>
<!-- body_eof //-->

<!-- jQuery/Plugins -->
<script src="includes/javascript/jquery-1.10.2.min.js"></script>
<script src="includes/javascript/jquery.tabify.source.js"></script>
<script src="includes/javascript/jquery_admin_menu.js"></script>

<!-- footer //-->
<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>
<!-- footer_eof //-->
<br>
</body>
</html>
<?php require(DIR_WS_INCLUDES . 'application_bottom.php'); 